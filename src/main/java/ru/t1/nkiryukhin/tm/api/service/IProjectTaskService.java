package ru.t1.nkiryukhin.tm.api.service;

import ru.t1.nkiryukhin.tm.exception.AbstractException;

public interface IProjectTaskService {

    void bindTaskToProject(String userId, String projectId, String taskId) throws AbstractException;

    void removeProjectById(String userId, String projectId) throws AbstractException;

    void unbindTaskFromProject(String userId, String projectId, String taskId) throws AbstractException;

}
