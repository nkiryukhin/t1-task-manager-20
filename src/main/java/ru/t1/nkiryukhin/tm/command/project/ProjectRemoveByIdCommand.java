package ru.t1.nkiryukhin.tm.command.project;

import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class ProjectRemoveByIdCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove project by id";
    }

    @Override
    public String getName() {
        return "project-remove-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY ID]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        final Project project = getProjectService().findOneById(id);
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
