package ru.t1.nkiryukhin.tm.api.service;

import ru.t1.nkiryukhin.tm.enumerated.Role;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.user.AccessDeniedException;
import ru.t1.nkiryukhin.tm.model.User;

public interface IAuthService {

    void checkRoles(Role[] roles) throws AbstractException;

    User registry(String login, String password, String email) throws AbstractException;

    void login(String login, String password) throws AbstractException;

    void logout();

    boolean isAuth();

    String getUserId() throws AccessDeniedException;

    User getUser() throws AbstractException;

}