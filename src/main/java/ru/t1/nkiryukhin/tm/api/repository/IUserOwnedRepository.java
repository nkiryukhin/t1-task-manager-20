package ru.t1.nkiryukhin.tm.api.repository;

import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.exception.field.UserIdEmptyException;
import ru.t1.nkiryukhin.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    M add(String userId, M model) throws UserIdEmptyException;

    void clear(String userId) throws UserIdEmptyException;

    boolean existsById(String userId, String id) throws AbstractFieldException;

    List<M> findAll(String userId) throws UserIdEmptyException;

    List<M> findAll(String userId, Comparator<M> comparator) throws UserIdEmptyException;

    M findOneById(String userId, String id) throws UserIdEmptyException, AbstractFieldException;

    M findOneByIndex(String userId, Integer index) throws AbstractFieldException;

    int getSize(String userId) throws UserIdEmptyException;

    M removeById(String userId, String id) throws AbstractFieldException;

    M removeByIndex(String userId, Integer index) throws AbstractFieldException;

    M remove(String userId, M model) throws UserIdEmptyException;

}
