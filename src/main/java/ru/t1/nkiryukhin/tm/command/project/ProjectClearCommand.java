package ru.t1.nkiryukhin.tm.command.project;

import ru.t1.nkiryukhin.tm.exception.AbstractException;


public final class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[PROJECT CLEAR]");
        final String userId = getUserId();
        getProjectService().clear(userId);
    }

    @Override
    public String getDescription() {
        return "Remove all projects.";
    }

    @Override
    public String getName() {
        return "project-clear";
    }

}
