package ru.t1.nkiryukhin.tm.command.task;

import ru.t1.nkiryukhin.tm.exception.AbstractException;

public final class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Remove all tasks";
    }

    @Override
    public String getName() {
        return "task-clear";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CLEAR TASKS]");
        final String userId = getUserId();
        getTaskService().clear(userId);
    }

}
