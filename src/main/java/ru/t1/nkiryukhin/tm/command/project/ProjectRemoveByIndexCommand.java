package ru.t1.nkiryukhin.tm.command.project;

import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.model.Project;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class ProjectRemoveByIndexCommand extends AbstractProjectCommand {

    @Override
    public String getDescription() {
        return "Remove project by index";
    }

    @Override
    public String getName() {
        return "project-remove-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[REMOVE PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final Project project = getProjectService().findOneByIndex(index);
        final String userId = getUserId();
        getProjectTaskService().removeProjectById(userId, project.getId());
    }

}
