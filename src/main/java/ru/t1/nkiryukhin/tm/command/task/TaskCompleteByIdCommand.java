package ru.t1.nkiryukhin.tm.command.task;

import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.util.TerminalUtil;

public final class TaskCompleteByIdCommand extends AbstractTaskCommand {

    @Override
    public String getDescription() {
        return "Complete task by id";
    }

    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        final String userId = getUserId();
        getTaskService().changeTaskStatusById(userId, id, Status.COMPLETED);
    }

}
