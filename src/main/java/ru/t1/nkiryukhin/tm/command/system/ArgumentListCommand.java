package ru.t1.nkiryukhin.tm.command.system;

import ru.t1.nkiryukhin.tm.api.model.ICommand;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @Override
    public String getArgument() {
        return "-arg";
    }

    @Override
    public String getDescription() {
        return "Show argument list";
    }

    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    public void execute() {
        final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        for (final ICommand command : commands) {
            final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
