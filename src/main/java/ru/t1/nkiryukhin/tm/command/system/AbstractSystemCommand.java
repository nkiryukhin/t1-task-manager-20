package ru.t1.nkiryukhin.tm.command.system;

import ru.t1.nkiryukhin.tm.api.service.ICommandService;
import ru.t1.nkiryukhin.tm.command.AbstractCommand;
import ru.t1.nkiryukhin.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
