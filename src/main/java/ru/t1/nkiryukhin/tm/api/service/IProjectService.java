package ru.t1.nkiryukhin.tm.api.service;

import ru.t1.nkiryukhin.tm.enumerated.Status;
import ru.t1.nkiryukhin.tm.exception.AbstractException;
import ru.t1.nkiryukhin.tm.exception.field.AbstractFieldException;
import ru.t1.nkiryukhin.tm.model.Project;


public interface IProjectService extends IUserOwnedService<Project> {

    Project changeProjectStatusById(String userId, String id, Status status) throws AbstractException;

    Project changeProjectStatusByIndex(String userId, Integer index, Status status) throws AbstractException;

    Project create(String userId, String name, String description) throws AbstractFieldException;

    Project create(String userId, String name) throws AbstractFieldException;

    Project updateById(String userId, String id, String name, String description) throws AbstractException;

    Project updateByIndex(String userId, Integer index, String name, String description) throws AbstractException;

}
